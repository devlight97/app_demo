import React, { Component } from 'react'
import { Card, Checkbox, TextField, FormGroup, FormControlLabel, Button, FormLabel, AppBar } from '@material-ui/core';
import {connect} from 'react-redux'
import {
    actionCloseShareDialog
} from './action'
import {subscribeShareProduct} from '../containers/ListProduct/action'
import { BlackScreen, Dialog, Product, ShareButton } from './styled';

class ShareProductDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            productShared: null,
            emailToShare: ""
        }
    }

    // add a product and user shared it in db
    shareProduct() {
        this.props.shareProduct(this.state.emailToShare, this.state.productShared);
    }

    // close share product dialog
    closeShareDialog() {
        this.props.closeShareDialog();
    }

    // display product info in dialog
    showProduct(product) {
        if (product) {
            return (
                <div>
                    <FormLabel style={{fontWeight: "600"}}>Name:</FormLabel>
                    <p>{product.name}</p>

                    <FormLabel style={{fontWeight: "600"}}>Brand:</FormLabel>
                    <p>{product.brand}</p>
                    
                    <img style={{width: "30%", marginLeft: "50%", transform: "translateX(-50%)"}} src={product.image} alt=""/><br/>

                    <FormLabel style={{fontWeight: "600"}}>Price:</FormLabel>
                    <p>{product.price}</p>
                </div>
            );
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps.productShared);
        
        this.setState({
            productShared: nextProps.productShared
        });
    }

    componentDidMount() {
        this.setState({
            productShared: this.props.productShared
        });
    }

  render() {
      let product = this.state.productShared
    return (
      <div>
        <BlackScreen onClick={() => this.closeShareDialog()}>
            <Dialog onClick={evt => evt.stopPropagation()}>
                <Card style={{
                    overflowY: "auto",
                    height: "100%"
                }}>
                    <AppBar position="static" style={{marginBottom: "1em", textAlign: "center", height: "3em", padding: "1em"}}>SHARE DIALOG</AppBar>
                    
                    <FormGroup style={{width: "100%"}}>
                        <TextField name="emailToShare" onChange={evt => {this.setState({[evt.target.name]: evt.target.value})}} placeholder="Fill email you want to share..." style={{marginLeft: "2em" ,width: "70%"}}></TextField>
                    </FormGroup>
                    <Product>
                        {this.showProduct(product)}
                    </Product>
                </Card>
                <Card>
                    <ShareButton>
                        <Button color="primary" variant="contained" onClick={() => {
                            this.shareProduct();
                            this.closeShareDialog();
                        }}>Share</Button>
                    </ShareButton>
                </Card>
            </Dialog>
        </BlackScreen>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        productShared: state.component.productShared
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        shareProduct: (email, productShared) => subscribeShareProduct(dispatch, email, productShared),
        closeShareDialog: () => actionCloseShareDialog(dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShareProductDialog);
