import * as TYPES from './constants'
import { Meteor } from 'meteor/meteor'

// add a product liked
export const actionAddLikedProduct = (productId) => {
    const user = { ...Meteor.user() }
    user.profile.product.liked.push(productId);
    Meteor.call('users.update', user, err => console.log(err));
}
// unlike a product
export const actionRemoveLikedProduct = (productId) => {
    const user = { ...Meteor.user() }
    const indexDelete = user.profile.product.liked.indexOf(productId);
    user.profile.product.liked.splice(indexDelete, 1);
    Meteor.call('users.update', user, err => console.log(err));
}

// open share product dialog
export const actionOpenShareDialog = (dispatch, productShareId) => {
    dispatch({
        type: TYPES.OPEN_SHARE_DIALOG,
        isShare: true,
        productShareId,
    });
}

// close share product dialog
export const actionCloseShareDialog = (dispatch) => {
  dispatch({
      type: TYPES.CLOSE_SHARE_DIALOG,
      isShare: false,
  });
}

// open list user shared product dialog
export const actionOpenShowOwnerDialog = (dispatch, owners) => {
    dispatch({ type: TYPES.OPEN_OWNER_DIALOG, owners });
}

// close list user shared product dialog
export const actionCloseShowOwnerDialog = (dispatch) => {
  dispatch({ type: TYPES.CLOSE_OWNER_DIALOG });
}
