import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import { IconButton } from '@material-ui/core';


const options = [
  'Choose a list',
  'List of products in store',
  'List of products to be shared',
  'List of products you liked',
];

class MenuBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      selectedIndex: 0,
    };
  }


  handleClickListItem = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  // set list that user just have chosen
  handleMenuItemClick = (event, index) => {
    this.setState({ selectedIndex: index, anchorEl: null });
    this.props.selectListToFill(index);
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;

    return (
      <IconButton color="inherit" aria-label="Open drawer">
        <MenuIcon onClick={evt => {this.handleClickListItem(evt)}}/>
        <div>
          <Menu
            id="lock-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={this.handleClose}
          >
            {options.map((option, index) => (
              <MenuItem
                key={option}
                disabled={index === 0}
                onClick={event => this.handleMenuItemClick(event, index)}
              >
                {option}
              </MenuItem>
            ))}
          </Menu>
        </div>
      </IconButton>
        
    );
  }
}


export default MenuBar;
