import * as TYPES from './constants'

const initialState = {
  products: [],
  currentUser: {},
  listUser: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
  case TYPES.LOAD_ALL_PRODUCTS:
    return { ...state, products: action.products }

  case TYPES.GET_CURRENT_USER:
    return { ...state, currentUser: action.user }

  case TYPES.GET_ALL_USER:
    return { ...state, listUser: action.users }

  default:
    return state
  }
}
