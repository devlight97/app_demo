import { Accounts } from 'meteor/accounts-base'
import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'

Meteor.methods({

  // create user account
  'users.insert': (user) => {
    check(user, Object);
    const { email, password, name } = user;

    const userExist = Accounts.findUserByEmail(email);

    if (!userExist) {
        const userId = Accounts.createUser({
            email,
            password,
            profile: {
                name,
                product: {
                    liked: [],
                },
            },
          });
        return userId;
    }
    return null;
  },

  // update info user
  'users.update': (user) => {
    check(user, Object);
    const userExist = Meteor.users.findOne({ _id: user._id });
    if (userExist) {
      Meteor.users.update(user._id, {
        $set: {
          username: user.emails[0].address,
          profile: user.profile,
        },
      });
    }
  },
});
