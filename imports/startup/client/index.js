import Meteor from 'meteor/meteor';
import AppRouter from './AppRouter';
import { render } from 'react-dom';
import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from 'styled-components';

const theme = createMuiTheme({
    palette: {
      colors: {
        white: '#FFFFFF',
        black: '#000000',
        red: '#F44336',
        pink: '#E91E63',
        purple: '#9C27B0',
        deepPurple: '#673AB7',
        indigo: '#3F51B5',
        blue: '#2196F3',
        lightBlue: '#03A9F4',
        cyan: '#00BCD4',
        teal: '#009688',
        green: '#4CAF50',
        lightGreen: '#8BC34A',
        lime: '#CDDC39',
        yellow: '#FFEB3B',
        amber: '#FFC107',
        orange: '#FF9800',
        deepOrange: '#FF5722',
        brown: '#795548',
        grey: '#9E9E9E',
        blueGrey: '#607D8B',
        blueviolet: '#8a2be2',
      },
    },
});

window.__theme = theme;

Meteor.Meteor.startup(() => {
  let app = document.getElementById('render-target');
  if (!app) {
      app = document.createElement('div');
      app.setAttribute('id', 'render-target');
      document.body.appendChild(app);
  }
  const appRouter = (
    <MuiThemeProvider theme={theme}>
      <ThemeProvider theme={theme}><AppRouter /></ThemeProvider>
    </MuiThemeProvider>
  );
  render(appRouter, app);
});
